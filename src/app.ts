import { Game } from './model';
import config from './config/config.json';
import * as readline from 'readline';

function play(input: Array<string>, winCommission: number, placeCommission: number, exactaCommission: number) {
    let game = new Game(input, winCommission, placeCommission, exactaCommission);
    game.initialize();
    console.log(game.handleWin());
    console.log(game.handlePlace());
    console.log(game.handleExacta());
}

function start() {
    if (config != null) {
        if (config.offline === true) {  // run with dummy data
            const input: Array<string> = config.dummyData2;     // change here
            if (validateInput(input) === true) {
                play(input, config.winCommission, config.placeCommission, config.exactaCommission);
            }
        }
        else {
            let rl = readline.createInterface({     //accept user input
                input: process.stdin,
                output: process.stdout
            });

            rl.question('Input the bets in format Bet:W:1:3,Bet:P:1:31,Bet:E:1,2:13. Separate bets with a space. Last line should be the result in format Result:2:3:1 ', (answer: string) => {
                const inputList:Array<string> = answer.split(' ');
                if (validateInput(inputList) === true) {
                    play(inputList, config.winCommission, config.placeCommission, config.exactaCommission);
                }
                rl.close();
            });
        }
    }
}

export function validateInput(inputList: Array<string>): boolean {
    let toReturn: boolean = true;

    const wpRegex = /(bet)(:)[wp](:)\d+(:)\d+/i;
    const eRegex = /(bet)(:)[e](:)\d+(,)\d+(:)\d+/i;;
    const resultRegex = /(result)(:)\d+(:)\d+(:)\d+/i;

    let bets: Array<string> = [];

    if (inputList.length > 0) {
        for (let i = 0; i < inputList.length - 1; i++) {
            if (wpRegex.test(inputList[i]) === true || eRegex.test(inputList[i]) === true) {
                toReturn = true;
            }
            else {
                toReturn = false;
                console.log("Bet input is not correct. Exiting",inputList[i].toString());
                break;
            }
        }

        if (toReturn === true) {
            if (resultRegex.test(inputList[inputList.length - 1]) === false) {
                console.log("Result input is not correct. Exiting");
                toReturn = false;
            }
        }
    }
    else {
        console.log("Not enough data. Exiting");
        toReturn = false;
    }
    return toReturn;
}

start();
