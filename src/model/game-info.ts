import { BetInfo } from './bet-info';
import {ResultInfo} from './result-info';

export class GameInfo {
    private _win: Array<BetInfo>;
    private _place: Array<BetInfo>;
    private _exacta: Array<BetInfo>;
    private _ignoreList: Array<string>;
    private _result!: ResultInfo;

    get win(): Array<BetInfo> {
        return this._win;
    }

    set win(value: Array<BetInfo>) {
        this._win = value;
    }

    get place(): Array<BetInfo> {
        return this._place;
    }

    set place(value: Array<BetInfo>) {
        this._place = value;
    }

    get exacta(): Array<BetInfo> {
        return this._exacta;
    }

    set exacta(value: Array<BetInfo>) {
        this._exacta = value;
    }

    get ignoreList(): Array<string> {
        return this._ignoreList;
    }

    set ignoreList(value: Array<string>) {
        this._ignoreList = value;
    }

    get result(): ResultInfo {
        return this._result;
    }

    set result(value: ResultInfo) {
        this._result = value;
    }

    constructor() {
        this._win = new Array<BetInfo>();
        this._place = new Array<BetInfo>();
        this._exacta = new Array<BetInfo>();
        this._ignoreList = new Array<string>();
    }
}
