import { GameInfo } from './game-info';
import { BetInfo } from './bet-info';
import { ResultInfo } from './result-info';

export class Game {

    private bets: Array<string>;    // input bets
    private consolidatedGameInfo!: GameInfo;    // process the bets
    private winCommission: number;
    private placeCommission: number;
    private exactaCommission: number;

    constructor(bets: Array<string>, winCommission: number, placeCommission:number, exactaCommission:number ) {
        this.bets = bets;
        this.consolidatedGameInfo = new GameInfo;
        this.winCommission = winCommission;
        this.placeCommission = placeCommission;
        this.exactaCommission = exactaCommission;
    }

    /**
     * preprocess the data - arrange data in proper buckets.
     */
    public initialize(): void {
        for (const bet of this.bets) {
            const currentBetList:Array<string> = bet.split(':');
            if (currentBetList.length == 4) {
                if (currentBetList[0].toLowerCase() === 'bet') {
                    const betAmount = +currentBetList[3];
                    if (typeof betAmount === 'number' && betAmount > 0) {
                        const betInfo: BetInfo  = new BetInfo(currentBetList[2], betAmount);
                        switch(currentBetList[1].toLowerCase().trim()) {
                            case 'w':
                                this.consolidatedGameInfo.win.push(betInfo);
                                break;
                            case 'p':
                                this.consolidatedGameInfo.place.push(betInfo);
                                break;
                            case 'e':
                                this.consolidatedGameInfo.exacta.push(betInfo);
                                break;
                        }
                    }
                    else {
                        this.consolidatedGameInfo.ignoreList.push(bet);
                    }
                }
                else if (currentBetList[0].toLowerCase() === 'result') {
                    this.consolidatedGameInfo.result = new ResultInfo(currentBetList[1], currentBetList[2], currentBetList[3]);
                }
                else {
                    this.consolidatedGameInfo.ignoreList.push(bet);
                }
            }
            else {
                this.consolidatedGameInfo.ignoreList.push(bet);
            }
        }

        if (this.consolidatedGameInfo.ignoreList.length > 0) {
            console.log(`${this.consolidatedGameInfo.ignoreList.length} bets were ignored because they were not well formatted`);
        }

    }

    /**
     * handle win bets
     */
    public handleWin(): string {
        let toReturn: string;
        if (this.consolidatedGameInfo.win.length > 0 && this.consolidatedGameInfo.result != null) {
            let totalPot: number = 0;
            let winnersContribution: number = 0;
            for (const win of this.consolidatedGameInfo.win) {
                totalPot += win.stake;
                if (win.selection === this.consolidatedGameInfo.result.firstPlace) {
                    winnersContribution += win.stake;
                }
            }

            if (winnersContribution > 0) {
                const remainingPool = totalPot * (1 - this.winCommission);
                const returnsRatio = (remainingPool/winnersContribution).toFixed(2);
                toReturn = `Win:${this.consolidatedGameInfo.result.firstPlace}:$${returnsRatio}`;
            }
            else {
                toReturn = 'No one won on Winners.';
            }
        }
        else {
            toReturn = 'No one played Winners.';
        }
        return toReturn;

    }

    /**
     * handle place bets
     */
    public handlePlace(): string {
        let toReturn: string;
        if (this.consolidatedGameInfo.place.length > 0 && this.consolidatedGameInfo.result != null) {
            let totalPot: number = 0;
            let firstPlaceStake: number = 0;
            let secondPlaceStake: number = 0;
            let thirdPlaceStake: number = 0;

            for (const item of this.consolidatedGameInfo.place) {
                totalPot += item.stake;
                switch (item.selection) {
                    case this.consolidatedGameInfo.result.firstPlace:
                        firstPlaceStake += item.stake;
                        break;
                    case this.consolidatedGameInfo.result.secondPlace:
                        secondPlaceStake += item.stake;
                        break;
                    case this.consolidatedGameInfo.result.thirdPlace:
                        thirdPlaceStake += item.stake;
                        break;
                }
            }


            const remainingPool: number = totalPot * (1 - this.placeCommission);
            const partialPool: number = remainingPool / 3;
            //todo - add a check - if stake is 0
            const firstPlaceReturnsRatio: string = firstPlaceStake > 0 ? `$${(partialPool/ firstPlaceStake).toFixed(2)}` : 'No one bet on first place';
            const secondPlaceReturnsRatio: string = secondPlaceStake > 0 ? `$${(partialPool / secondPlaceStake).toFixed(2)}` : 'No one bet on second place';
            const thirdPlaceReturnsRatio: string = thirdPlaceStake > 0 ? `$${(partialPool / thirdPlaceStake).toFixed(2)}` : 'No one bet on third place';

            toReturn = `Place:${this.consolidatedGameInfo.result.firstPlace}:${firstPlaceReturnsRatio}\nPlace:${this.consolidatedGameInfo.result.secondPlace}:${secondPlaceReturnsRatio}\nPlace:${this.consolidatedGameInfo.result.thirdPlace}:${thirdPlaceReturnsRatio}`
        }
        else {
            toReturn = 'No one played Place.';
        }
        return toReturn;

    }

    /**
     * handle exacta bets
     */
    public handleExacta(): string {
        let toReturn: string;
        if (this.consolidatedGameInfo.exacta != null && this.consolidatedGameInfo.exacta.length > 0 && this.consolidatedGameInfo.result != null) {
            let totalPot: number = 0;
            let winnersContribution: number = 0;
            for (const item of this.consolidatedGameInfo.exacta) {
                totalPot += item.stake;

                const winSelectionList: Array<string> = item.selection.split(',');
                if (winSelectionList[0] === this.consolidatedGameInfo.result.firstPlace && winSelectionList[1] === this.consolidatedGameInfo.result.secondPlace) {
                    winnersContribution += item.stake;
                }
            }

            if (winnersContribution > 0) {
                const remainingPool: number = totalPot * (1- this.exactaCommission);
                const returnsRatio = (remainingPool/winnersContribution).toFixed(2);
                toReturn = `Exacta:${this.consolidatedGameInfo.result.firstPlace},${this.consolidatedGameInfo.result.secondPlace}:$${returnsRatio}`;
            }
            else {
                toReturn = 'No one won on Exacta.';
            }
        }
        else {
            toReturn = 'No one played Exacta.';
        }
        return toReturn;

    }


}
