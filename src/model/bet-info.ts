export class BetInfo {

    private _selection: string;
    private _stake: number;

    get stake(): number {
        return this._stake;
    }
    get selection(): string {
        return this._selection;
    }

    constructor (selection: string, stake: number) {
        this._selection = selection;
        this._stake = stake;
    }
}
