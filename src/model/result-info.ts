export class ResultInfo {
    private _firstPlace: string;
    private _secondPlace: string;
    private _thirdPlace: string;

    constructor(firstPlace: string, secondPlace: string, thirdPlace: string) {
        this._firstPlace = firstPlace;
        this._secondPlace = secondPlace;
        this._thirdPlace = thirdPlace;
    }

    get firstPlace(): string {
        return this._firstPlace;
    }

    get secondPlace(): string {
        return this._secondPlace;
    }

    get thirdPlace(): string {
        return this._thirdPlace;
    }

}
