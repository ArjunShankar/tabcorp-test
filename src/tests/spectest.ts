import * as app from '../app';
import config from '../config/config.json';
import { Game } from '../model/game';


describe('data validation', function() {
    it('correct data', function() {
        expect(app.validateInput(config.dummyData2)).toBe(true);
    });

    it('incorrect data', function() {
        expect(app.validateInput(config.dummyData)).toBe(false);
    });
});

describe('game', function() {
  /*  var game: Game;
    beforeEach( function(){
        game = new Game(config.winData, config.winCommission, config.placeCommission, config.exactaCommission);
        game.initialize();
    });*/

    it('win - 1', function() {
        let game = new Game(config.winData, config.winCommission, config.placeCommission, config.exactaCommission);
        game.initialize();
        expect(game.handleWin()).toBe('Win:2:$2.61');
    });
    it('win - no one played', function() {
        let game = new Game(config.winDataNoWinners, config.winCommission, config.placeCommission, config.exactaCommission);
        game.initialize();
        expect(game.handleWin()).toBe('No one won on Winners.');
    });
    it('win - bad input', function() {
        let game = new Game(config.winDataBadInput, config.winCommission, config.placeCommission, config.exactaCommission);
        game.initialize();
        expect(game.handleWin()).toBe('No one played Winners.');
    });

});
